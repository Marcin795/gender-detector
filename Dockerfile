FROM gradle:6.8.3-jdk11 AS build
WORKDIR /app
COPY build.gradle .
COPY src ./src
RUN gradle clean bootJar

FROM openjdk:11.0.4-jre-slim
COPY --from=build /app/build/libs/app*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]