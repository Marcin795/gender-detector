package com.example.genderdetector.algorithm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import static com.example.genderdetector.Gender.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GenderDetectorTest {

    @Autowired
    private GenderDetector genderDetector;

    @Test
    void detectFirstFemale() {
        var name = "Ada Jan Paweł";
        assertEquals(FEMALE, genderDetector.detectFirst(name));
    }

    @Test
    void detectFirstMale() {
        var name = "Jan Ada Paweł";
        assertEquals(MALE, genderDetector.detectFirst(name));
    }

    @Test
    void detectFirstInconclusive() {
        var name = "Hans Paweł Ada";
        assertEquals(INCONCLUSIVE, genderDetector.detectFirst(name));
    }

    @Test
    void detectMajority() {
        var name = "Ada Jan Paweł";
        assertEquals(MALE, genderDetector.detectMajority(name));
    }

    @Test
    void detectMajorityWithGarbage() {
        var name = "Ada Jan Anna Garbage";
        assertEquals(FEMALE, genderDetector.detectMajority(name));
    }

    @Test
    void detectInconclusiveMajority() {
        var name = "Ada Jan Paweł Anna";
        assertEquals(INCONCLUSIVE, genderDetector.detectMajority(name));
    }

    @Test
    void detectInconclusiveFirstOnly() {
        var name = "Hans Jan Paweł Anna";
        assertEquals(INCONCLUSIVE, genderDetector.detectFirst(name));
    }

}