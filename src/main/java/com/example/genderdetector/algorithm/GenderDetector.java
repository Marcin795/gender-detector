package com.example.genderdetector.algorithm;

import com.example.genderdetector.Gender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

import static com.example.genderdetector.Gender.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.function.Function.identity;

@Component
public class GenderDetector {

    private final long maleTagsCount;
    private final long femaleTagsCount;

    private final ResourceLoader resourceLoader;

    private final String maleTags;
    private final String femaleTags;

    public GenderDetector(@Value("${male.tags.filename}") String maleTags, @Value("${female.tags.filename}") String femaleTags, ResourceLoader resourceLoader) {
        this.maleTags = maleTags;
        this.femaleTags = femaleTags;
        this.resourceLoader = resourceLoader;
        maleTagsCount = getCount(maleTags);
        femaleTagsCount = getCount(femaleTags);
    }

    private long getCount(String filename) {
        try (var stream = new BufferedReader(new InputStreamReader(resourceLoader.getResource(filename).getInputStream(), UTF_8)).lines()) {
            return stream.count();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Gender detectFirst(String name) {
        return detect(name.split(" ")[0]);
    }

    public Gender detectMajority(String name) {
        var dupa = Arrays.stream(name.split(" "))
                .map(this::detect)
                .filter(detected -> detected != INCONCLUSIVE)
                .collect(Collectors.groupingBy(identity(), Collectors.counting()));
        return dupa.entrySet().stream()
                .reduce(GenderDetector::uniqueMax)
                .map(Map.Entry::getKey)
                .orElse(INCONCLUSIVE);
    }

    private Gender detect(String name) {
        var isMale = find(maleTags, name, 0, maleTagsCount - 1);
        var isFemale = find(femaleTags, name, 0, femaleTagsCount - 1);
        if (isMale == isFemale)
            return INCONCLUSIVE;
        if (isMale)
            return MALE;
        return FEMALE;
    }

    private boolean find(String filename, String name, long from, long to) {
        var middleOfInterval = (to - from) / 2 + from;
        var found = getLine(filename, middleOfInterval);
        if (name.compareToIgnoreCase(found) == 0)
            return true;
        if (to <= from)
            return false;
        if (name.compareToIgnoreCase(found) > 0)
            from = middleOfInterval + 1;
        else if (name.compareToIgnoreCase(found) < 0)
            to = middleOfInterval - 1;
        return find(filename, name, from, to);
    }

    private String getLine(String filename, long lineNumber) {
        try (var lines = new BufferedReader(new InputStreamReader(resourceLoader.getResource(filename).getInputStream(), UTF_8)).lines()) {
            return lines.skip(lineNumber)
                    .findFirst()
                    .orElseThrow();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Map.Entry<Gender, Long> uniqueMax(Map.Entry<Gender, Long> entry1, Map.Entry<Gender, Long> entry2) {
        if (entry1.getValue().equals(entry2.getValue()))
            return new AbstractMap.SimpleImmutableEntry<>(INCONCLUSIVE, 0L);
        else
            return Collections.max(asList(entry1, entry2), Comparator.comparingLong(Map.Entry::getValue));
    }

}
