package com.example.genderdetector;

public enum Gender {

    MALE,
    FEMALE,
    INCONCLUSIVE

}
