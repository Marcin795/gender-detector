package com.example.genderdetector.controller;

import com.example.genderdetector.Gender;
import com.example.genderdetector.algorithm.GenderDetector;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("gender-detector")
public class GenderDetectorController {

    private final GenderDetector genderDetector;

    public GenderDetectorController(GenderDetector genderDetector) {
        this.genderDetector = genderDetector;
    }

    @GetMapping("/first-only/{name}")
    public @ResponseBody Gender firstOnly(@PathVariable String name) {
        return genderDetector.detectFirst(name);
    }

    @GetMapping("/majority/{name}")
    public @ResponseBody Gender majority(@PathVariable String name) {
        return genderDetector.detectMajority(name);
    }

}
